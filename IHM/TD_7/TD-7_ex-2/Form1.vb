﻿Public Class Form1

    Private Sub TextBox1_MouseMove(sender As Object, e As MouseEventArgs) Handles TextBox1.MouseMove
        Dim TextBox1 As TextBox = sender
        Dim rep As DragDropEffects

        If e.Button = MouseButtons.Left Then
            TextBox1.DoDragDrop(TextBox1.Text, DragDropEffects.Move)
            rep = TextBox1.DoDragDrop(TextBox2.Text, DragDropEffects.Move)

            If rep = DragDropEffects.Move Then
                TextBox1.Clear()
            End If

        End If

    End Sub
    Private Sub TextBox2_DragEnter(sender As Object, e As DragEventArgs) Handles TextBox2.DragEnter

        If e.Data.GetDataPresent(DataFormats.Text) = True Then
            e.Effect = DragDropEffects.Move

        Else
            e.Effect = DragDropEffects.None

        End If
    End Sub

    Private Sub TextBox2_DragDrop(sender As Object, e As DragEventArgs) Handles TextBox2.DragDrop
        Dim Textbox_1 As TextBox = sender

        Textbox_1.Text = e.Data.GetData(DataFormats.Text)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox2.AllowDrop = True
    End Sub
End Class
