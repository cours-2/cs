#ifndef PERSONNAGE_H
#define PERSONNAGE_H


class Personnage
{
    public:
        Personnage();
        ~Personnage();

    protected:

    private:
        string name;
        short vie;
        short exp;
        short attaque;
};

#endif // PERSONNAGE_H
