#include <iostream>
#include <stdio.h>
#include <windows.h>


using namespace std;

/*la consigne est de creer un jeu de mastermind*/

/*tout d�abord voici la fonction qui permet de generer automatiquement
les numeros du jeu a trouver : */

/* fonction qui determine si tous les chiffres saisies sont bien
compris entre 1 et 8 inclus */

int BonneSaisie(int n,int T[])
{ int i;
for (i=1;i<n+1;i++)
{if (T[i]<0 || T[i]>8)
return 0;}

return 1;}

/*ci dessous la fonction que faire qui nous servira ulterieurement*/

int QueFaire()
{int rep;
printf("que voulez vous faire ? :\n\n");
printf(" JOUER 1\n");
printf(" REJOUER LE MEME 2\n");
printf(" DONNER LA SOLUTION 3\n");
printf(" QUITTER 4\n\n");
scanf("%d",&rep);
return rep;}

/*passons maintenant a la fonction qui genere aleatoirement les numeros*/

void SaisieInitiale(int n,int T[])
{ int i;
for (i=1;i<n+1;i++)
{T[i]=/*(rand()%9)*/( (rand()%8)+1);}}

/*ce qui suit est une aide pour nous il sert afficher la tableau initiale*/

void AfficherSaisieInitiale(int n, int T[])
{int i;
for (i=1;i<n+1;i++)
{printf("%d ",T[i]);}
printf("\n\n\n");}

/*passons a la fonction qui va demander au joueur les numero qu�il
souhaite saisir*/

void SaisieJoueur(int n, int Tbis[])
{int i;
printf("\n\n entrez vos cinq valeur separes par un espace :\n");
for (i=1;i<n+1;i++)
{scanf("%d",&Tbis[i]);}
if (0==BonneSaisie(n,Tbis))
{printf("une ou plusieurs des valeurs saisies sont fausse, nouvelle saisie :");
SaisieJoueur(n, Tbis);}}

/*passons a la fonction copie qui copie le tableau initiale
affin qu�on puisse y travailler dessus*/

/*cette version de copie ne marche pas*/
/*void copie(int n,int T[])
{int Tcop[5];
int i;
for (i=1;i<n+1;i++)
{Tcop[i]=T[i];}}*/

//nouvelle version

void Copie(int n, int T[],int T1[])
{int i;
for (i=1;i<n+1;i++)
{T1[i] = T[i];}}

/*passons maintenant aux fonctions plus pr�cise
qui va nous indiquer
combien de chiffres sont bien plac� */

int BienPlace(int n, int T[], int T1[])
{int i;
int bp=0;
for (i=1;i<n+1;i++)
{if (T[i]==T1[i])
{bp+=1;
T[i]=9; //on remplace par 9 pour ne pas le compter deux fois
T1[i]=9; }

} //ici aussi
return bp;}

/*maintenant les mal plac�*/

int MalPlace(int n,int T[],int T1[])
{int i,j,mp=0;
for (i=1;i<n+1;i++)
{ for (j=1;j<n+1;j++)
    if (T[i]==T1[j]/*&& i!=j */&& T[i]!=9 && T1[j]!=9)
        {
            mp+=1;
T[i] = 9; //on remplace ici aussi par 9
T1[j] = 9;}}} //ici aussi

/*ecrivons une fonction qui determine si le ta
bleau comporte que des 0*/

int FIN(int n, int T[])
{int i;
for (i=1;i<n+1;i++)
{if (T[i] == 0) return 0;}
return 1;}

/*attaquons le gros du sujet mais toujour pas la fonction main*/

void mastermind()
{int nbfoix=0; int T[5];int bpf=0;int nbfoixmax=12;
SaisieInitiale(5,T); //saisie aleatoirement les numero
AfficherSaisieInitiale(5,T); //a ne pas laisser dans la version finale

while (bpf != 5)
{ int Tbis[5];
SaisieJoueur(5,Tbis);
if (0==FIN(5,Tbis)) { printf("vous avez decide d arreter a bientot\n");break;}
nbfoix+=1;
// AfficherSaisieInitiale(5,Tbis);
/*int Tcop[5];
copie(5,T);*/
int Tcop[5];
/*debut de la copie manuelle*/
/* Tcop[1]=T[1];
Tcop[2]=T[2];
Tcop[3]=T[3];
Tcop[4]=T[4];
Tcop[5]=T[5];*/

// AfficherSaisieInitiale(5,Tcop);

/*fin de la copie manuelle*/
Copie(5,T,Tcop);

bpf=BienPlace(5,Tcop,Tbis);
printf("il y a %d chiffres bien places"
,bpf);
printf("\n");
printf("il y a %d chiffres mal places\n",MalPlace(5,Tcop,Tbis));
if (nbfoix==nbfoixmax) {printf("vous avez expirer votre nombre de fois\n");
break;}
if (bpf ==5) {printf("VOUS AVEZ GAGNER\n");}

}}

/*maintenant la fonction main*/

int main()
{printf("Welcome to MasterMind\n\n");
int nbpr =9; // pour 8 ca met des zero

/* while (QueFaire()==1) fonction du deb
ut
{srand(nbpr);
nbpr+=1;
mastermind();
} */

/*ameliorons la fonction pour nous donner la posssibilit�
de rejouer le mastermind precedent ou bien d�imprimer la solution*/
int rep; int Tres[5];
while ((rep = QueFaire())!=4)
{srand(nbpr);
if (rep==1)
{/*srand(nbpr);*/
nbpr+=1;
mastermind();}
else if(rep==2)
{srand(nbpr-1);
/*nbpr+=1;*/
mastermind();}
else if (rep==3)
{ srand(nbpr-1);
SaisieInitiale(5,Tres);
printf("\n\n la solution etait : ");
AfficherSaisieInitiale(5,Tres);}

}

printf("\n A TRES BIENTOT SUR MASTERMIND \n\n\n\n");

system("PAUSE");
return 0;}

void textcolor(unsigned short color) { //les couleurs
    HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hcon,color);
}

int main();

  int couleur,background,foreground;
  for(int i=0;i<=255;i++) {
         textcolor(i);
         printf("i=%d  ",i);
  }
  /* pour d�finir une couleur
  couleurs : Arri�re plan =4 bits de poids fort Avant plan=4bits de poids faible
    0 = Black	8 = Gray
    1 = Blue	9 = Light Blue
    2 = Green	10 = Light Green
    3 = Aqua	11 = Light Aqua
    4 = Red	    12 = Light Red
    5 = Purple	13 = Light Purple
    6 = Yellow	14 = Light Yellow
    7 = White	15 = Bright White
    system("color 02"); 0 correspond au fond et 2 au texte */
  background= 1; //bleu
  foreground=14; //jaune
  couleur=background*16 + foreground;
  textcolor(couleur);
  cout<<"\nVoici un exemple\n";
  textcolor(12); cout<<"A";textcolor(0);cout<<" ";
  textcolor(10); cout<<"A";textcolor(0);cout<<" ";
  textcolor(9); cout<<"A";textcolor(0);cout<<" ";
  textcolor(14); cout<<"A";textcolor(0);cout<<" ";
  textcolor(13); cout<<"A";textcolor(0);cout<<" ";
  textcolor(15); cout<<"A";textcolor(0);cout<<" ";

  cout<<endl<<"Et un autre"<<endl;
  textcolor(12*16); cout<<"  ";textcolor(0);cout<<" ";
  textcolor(10*16); cout<<"  ";textcolor(0);cout<<" ";
  textcolor(9*16); cout<<"  ";textcolor(0);cout<<" ";
  textcolor(14*16); cout<<"  ";textcolor(0);cout<<" ";
  textcolor(13*16); cout<<"  ";textcolor(0);cout<<" ";
  textcolor(15*16); cout<<"  ";textcolor(0);cout<<" ";
 return 0;
}
