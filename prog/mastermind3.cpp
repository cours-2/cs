#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>

#define RED 12 // 3
#define BLUE 9 // 2
#define WHITE 15 // 1
#define GREEN 10 // 4
#define YELLOW 14  // 5
#define PURPLE 5 // 6
#define BLACK 0 // 0
#define GREY 7 // 7

#define RETURN 53
#define LEFT 52
#define RIGHT  54
#define UP 56
#define DOWN 50
#define F1 59

void Color(int couleurDuTexte,int couleurDeFond){
        HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
        SetConsoleTextAttribute(H,couleurDeFond*16+couleurDuTexte);
};

struct pion{
    int CouleurFront;
    int CouleurBack;
};

void initStructPion(struct pion pions[13][4])
{
    int x,y;
    for(y=0;y<13;y++){
        for(x=0;x<4;x++){
            pions[y][x].CouleurFront=0;
            pions[y][x].CouleurBack=0;
        }
    }
};

void initTab(int tab1[13][4])
{
    int x,y;
    for(y=0;y<13;y++){
        for(x=0;x<4;x++){
            tab1[y][x]=0;
        }
    }
};

void initCombinaison(int tab1[4])
{
    struct tm tm;
    srand(tm.tm_year);
    int tableau[6],i,x;
    tableau[0]=1;
    tableau[1]=2;
    tableau[2]=3;
    tableau[3]=4;
    tableau[4]=5;
    tableau[5]=6;
    for(i=0;i<4;i++){
        do{
            x=(rand()*6/RAND_MAX);
        }while(tableau[x]<1 || tableau[x]>6);
        tab1[i]=tableau[x];
          tableau[x]=0;
    }
}

void correctionLigne(struct pion pions[13][4],int tabCor[13][4], int ligne,int combinaison[4])
{
    int x,i=0,x1;
    for(x=0;x<4;x++){
        if(pions[ligne][x].CouleurFront==combinaison[x]){
            tabCor[ligne][i]=2;
            i++;
        }
    }
    if(i!=4){
        for(x=0;x<4;x++){
            for(x1=0;x1<4;x1++){
                if(pions[ligne][x].CouleurFront==combinaison[x1]&&x!=x1){
                    tabCor[ligne][i]=1;
                    i++;
                }
            }
        }
    }
};

void selection(struct pion pions[13][4],int *correction[13][4],int combinaison[4])
{
    int saisie,key=0,x,y,total=0,total1=0,ligne;
    for(y=0;y<13;y++){
        for(x=0;x<4;x++){
            if(pions[y][x].CouleurBack==7) ligne=y;
        }
    }
    while(saisie!=RETURN&&saisie!=LEFT&&saisie!=RIGHT&&saisie!=UP&&saisie!=DOWN&&saisie!=F1)
    {
        saisie=getch();
    }
    switch(saisie){
        case LEFT :
            key=4;
        break;
        case RIGHT :
            key=6;
        break;
        case UP :
            key=8;
        break;
        case DOWN :
            key=2;
        break;
        case RETURN :
            key=5;
        break;
        case F1 :
            key=10;
        break;
    }
    if(key!=5 && key!=10 && key!=11){
    for(y=0;y<13;y++){
        for(x=0;x<4;x++){
            if(pions[y][x].CouleurBack==7){
                if(key==6 && x<3){
                    pions[y][x].CouleurBack=0;
                    pions[y][x+1].CouleurBack=-7;
                }
                if(key==4 && x>0){
                    pions[y][x].CouleurBack=0;
                    pions[y][x-1].CouleurBack=-7;
                }
                if(key==8 && pions[y][x].CouleurFront<6){
                    pions[y][x].CouleurFront++;
                }
                if(key==2 && pions[y][x].CouleurFront>0){
                    pions[y][x].CouleurFront--;
                }
            }
        }
    }
    for(y=0;y<13;y++){
        for(x=0;x<4;x++){
            if(pions[y][x].CouleurBack==-7) pions[y][x].CouleurBack=7;
        }
    }
    }
    if(key==5){
        for(x=0;x<4;x++){
            if(pions[ligne][x].CouleurFront==0) total++;
            for(y=0;y<4;y++){
                if(pions[ligne][x].CouleurFront==pions[ligne][y].CouleurFront) total1++;
            }
        }
        if(total==0 && total1==4){
            for(y=0;y<13;y++){
                for(x=0;x<4;x++){
                    if(pions[y][x].CouleurBack==7){
                        correctionLigne(pions,correction,y,combinaison);
                        pions[y-1][x].CouleurBack=7;
                        pions[y][x].CouleurBack=0;
                    }
                }
            }
        }
    }
    if(key==10){
            Color(WHITE,BLACK);
            system("cls");
            Color(RED,BLACK);
            printf("- MASTERMIND 2 -\n\n\n\n");
            Color(WHITE,BLACK);
            printf("Instruction :\n\n- Pour changer la couleur d'un pion appuiez sur 8 et 2\n\n- Pour selection un autre pion appuiez sur 4 pour la gauche et 6 pour la droite\n\n- Pour valider la ligne appuiez sur 5 \n  (aucun pion ne doit etre de la meme couleur)\n\n- Pour reafficher cette aide appuiez sur F1\n\n- Un point rouge represente une couleur bien placee et un blanc une couleur\n  presente\n\n\n\n");
            system("pause");
    }
};

void affiche(struct pion pions[13][4],int correction[13][4],int combinaison[4],int cacheCombinaison){
    int x,y;
    Color(BLACK,BLACK);
    printf("  ");
    Color(GREY,GREY);
    printf("           \n");
    Color(BLACK,BLACK);
    printf("  ");
    Color(GREY,GREY);
    printf(" ");
    Color(BLACK,BLACK);
    printf(" ");
    if(cacheCombinaison==0){
    for(x=0;x<4;x++){
            switch(combinaison[x]){
                case 1 :
                Color(WHITE,BLACK);
                break;
                case 2 :
                Color(BLUE,BLACK);
                break;
                case 3 :
                Color(RED,BLACK);
                break;
                case 4 :
                Color(GREEN,BLACK);
                break;
                case 5 :
                Color(YELLOW,BLACK);
                break;
                case 6 :
                Color(PURPLE,BLACK);
                break;
            }
            printf("O");
            Color(WHITE,BLACK);
            printf(" ");
        }
    }else{
        Color(WHITE,BLACK);
        printf("X X X X ");
    }
            Color(GREY,GREY);
            printf(" ");
            Color(BLACK,BLACK);
            printf("\n  ");
            Color(GREY,GREY);
            printf("           \n");
    for(y=0;y<13;y++){
        Color(BLACK,BLACK);
        printf("  ");
        Color(GREY,GREY);
        printf(" ");
        Color(BLACK,BLACK);
        printf(" ");
        for(x=0;x<4;x++){
            switch(pions[y][x].CouleurFront){
                case 0 :
                Color(WHITE,pions[y][x].CouleurBack);
                printf(".");
                break;
                case 1 :
                Color(WHITE,pions[y][x].CouleurBack);
                printf("O");
                break;
                case 2 :
                Color(BLUE,pions[y][x].CouleurBack);
                printf("O");
                break;
                case 3 :
                Color(RED,pions[y][x].CouleurBack);
                printf("O");
                break;
                case 4 :
                Color(GREEN,pions[y][x].CouleurBack);
                printf("O");
                break;
                case 5 :
                Color(YELLOW,pions[y][x].CouleurBack);
                printf("O");
                break;
                case 6 :
                Color(PURPLE,pions[y][x].CouleurBack);
                printf("O");
                break;
            }
            Color(WHITE,BLACK);
            printf(" ");
        }
        Color(GREY,GREY);
        printf(" ");
        Color(BLACK,BLACK);
        printf(" ");
        for(x=0;x<4;x++){
            switch(correction[y][x]){
                case 0 :
                Color(BLACK,BLACK);
                break;
                case 1 :
                Color(WHITE,BLACK);
                break;
                case 2 :
                Color(RED,BLACK);
                break;
            }
            printf(".");
        }
        printf("\n");
    }
    Color(BLACK,BLACK);
    printf("  ");
    Color(GREY,GREY);
    printf("           \n");
}

int main()
{
    int combinaison[4],correction[13][4],gagner=0,x,y,totalPionRouge;
    initCombinaison(combinaison);
    initTab(correction);
    struct pion pions[13][4];
    initStructPion(pions);
    pions[12][0].CouleurBack=7;
    Color(RED,BLACK);
    printf("- MASTERMIND 2 -\n\n\n\n");
    Color(WHITE,BLACK);
    printf("Instruction :\n\n- Pour changer la couleur d'un pion appuiez sur 8 et 2\n\n- Pour selection un autre pion appuiez sur 4 pour la gauche et 6 pour la droite\n\n- Pour valider la ligne appuiez sur 5 \n  (aucun pion ne doit etre de la meme couleur)\n\n- Pour reafficher cette aide appuiez sur F1\n\n- Un point rouge represente une couleur bien placee et un blanc une couleur\n  presente\n\n\n\n");
    system("pause");
    do{
        system("cls");
        Color(RED,BLACK);
        printf("- MASTERMIND 2 -\n\n");
        affiche(pions,correction,combinaison,1);
        selection(pions,correction,combinaison);
        for(y=12;y>=0;y--){
            totalPionRouge=0;
            for(x=0;x<4;x++){
                if(correction[y][x]==2) totalPionRouge++;
            }
            if(totalPionRouge==4) gagner=1;
        }
        Color(WHITE,BLACK);
    }while(correction[0][0]==0 && gagner==0);
    if(gagner==1){
        system("cls");
        Color(RED,BLACK);
        printf("- MASTERMIND 2 -\n\n");
        affiche(pions,correction,combinaison,0);
        Color(WHITE,BLACK);
        printf("\nGAGNER !\n\nVous avez trouve la bonne combinaison !\n\n");
        system("pause");
    }else{
        system("cls");
        Color(RED,BLACK);
        printf("- MASTERMIND 2 -\n\n");
        affiche(pions,correction,combinaison,0);
        Color(WHITE,BLACK);
        printf("\nPerdu !\n\nVous n'avez pas trouver la bonne combinaison !\n\n");
        system("pause");
    }
    return 0;
}
