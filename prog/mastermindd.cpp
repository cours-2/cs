#include <iostream>
#include <ctime>    // pour la fonction time()
#include <cstdlib>  // pour la fonction srand() and rand()

using namespace std;

void accueil();
void prompt(int guess[], int n);
bool gaveOver(int guess[], int right[]);

int main(void)
{
    char symbol;
    int guess[5];
    int right[5];
    bool playAgain = true;
    bool won;
    int numGuesses;
    int maxGuesses;

    accueil();

    while (playAgain) {
        won = false;
        numGuesses = 1;
        maxGuesses = 10;

        // genere la bonne combinaison
        srand(time(0));
        for (int i = 0; i < 5; i++)
        {
            right[i] = rand() % 10;
        }
        // loop le temps que le joueur a les bonnes valeurs
        while (!won && numGuesses <= maxGuesses)
        {
              prompt(guess, numGuesses);
              won = gaveOver(guess, right);
              numGuesses++;
        }

        if (won)
        {
           cout << "Vous avez gagne! La bonne combinaison est bien: " << right[0] << " "
                << right[1] << " " << right[2] << " " << right[3] << " " << right[4]  << endl;
        }
        else
        {
            cout << "Vous avez perdu! La bonne combinaison etait : " << right[0] << " "
                 << right[1] << " " << right[2] << " " << right[3] << " " << right[4] << endl;
        }

        cout << "Voulez-vous rejouer? (O or N): ";

        cin >> symbol;

        playAgain = (symbol == 'O' || symbol == 'o') ? true : false;
    }
    cout <<" J'espere le jeu vous a plu! A bientot! "<< endl;
    return 0;
} // fin main

bool gaveOver(int guess[], int right[])
{
     int totalRight = 0;
     int rightColor = 0;
     int grab;
     bool exclude[5];
     bool excludeColor[5];
     bool inList = false;
 string result="_____";
     for (int i = 0; i < 5; i++)
     {
         exclude[i] = false;
         excludeColor[i] = false;
     }

     // loop pour determiner lesquels ont le bon numero et la bonne position
     for (int i = 0; i < 5; i++)
     {
         if (guess[i] == right[i])
         {
            totalRight++;
            result[i]='X';
            exclude[i] = true;
         }
     }

     // loop pour determiner si un numero est bon mais de mauvaise position
     for (int i = 0; i < 5; i++)
     {
         if (!exclude[i])
         {
            for (int j = 0; j < 5; j++)
            {
                if (!exclude[j] && i != j)
                {
                   if ((guess[i] == right[j]) && !excludeColor[j])
                   {
                      inList = true;
                      grab = j;
                   }
                }
            }

            if (inList)
            {
               rightColor++;
                result[i]='O';
               inList = false;
               excludeColor[grab] = true;
            }
         }



     }

     if (totalRight == 5)
        return true;
     else
     {
         for(int i = 0; i < totalRight; i++) {
            cout << "X ";
         }
         for(int i = 0; i < rightColor; i++){
            cout << "O ";
         }
         for(int i = 0; i < 5 - totalRight - rightColor; i++) {
            cout << "_ ";
         }
         cout<<"       "<<result<<endl;
        return false;
     }

} // fin gameOver

void prompt(int guess[], int n)
{
     cout << " Entrez 5 chiffres separement pour proposer votre combinaison (Tentative " << n << "): ";

     cin >> guess[0] >> guess[1] >> guess[2] >> guess[3] >> guess[4] ;


     for (int i = 0; i < 5; i++)
     {
        guess[i] %= 10;
     }

} // fin prompt

void accueil()
{
    cout << "=================================================== Mastermind Game ==================================================="<<  endl;
    cout << "" <<endl;
    cout << "  Trouvez la bonne combinaison de 5 chiffres determine par l'ordinateur" << endl;
    cout << " Apres chaque combinaison saisi vous verrez quels chiffres ne sont pas dans la combinaison"<< endl;
    cout << " quels chiffres sont bien ou mal places ou ceux que vous n'avez pas encore saisi" << endl;
    cout << " Les chiffres a saisir sont 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 " << endl;
    cout << "" <<endl;
    cout << "  Interpretation des resultats obtenus:" << endl;
    cout << " X -> pion de la combinaison en bonne position"<<endl;
    cout << " O -> pion de la combinaison en mauvaise position"<<endl;
    cout << " _ -> pion qui ne figure pas dans la combinaison"<<endl;
    cout << "" <<endl;
    cout << "  Vous avez le droit a 10 essais, attention ils sont comptes!" << endl;


}
