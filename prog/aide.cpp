#define TRUE 1;
#define FALSE 0;

void tri_a_bulle(int t[], int const n)
{

	int en_desordre = TRUE;
	while (en_desordre)
	{
		en_desordre = FALSE;
		for (int j = 0; j < n-1; j++)
		{
			if(t[j] > t[j+1])
			{
 				int tmp = t[j+1];
 				t[j+1] = t[j];
 				t[j] = tmp;

				en_desordre = TRUE;
 			}
		}
	}
}
