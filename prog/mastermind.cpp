    #include <stdio.h> // Fonctions getchar, printf()
    #include <stdlib.h> // Fonctions system()
    #include <string.h>
    #include <time.h>
    int menu();
    void jeu();
    void regles();
    // ---------------------------------------------------------------------------------------------------------------------------------------------
    void initCode(int *codeSecret);
    int main ()
    {
    int continuer = 1, choix = 0;
    int i;
      int *codeSecret[4];


      for(i=0; i<4; i++)
        printf("%c", codeSecret[i]);
    while (continuer) //Tant que continuer vaut vrai (1)
    {
        choix = menu();
        switch (choix)
        {
            case 1:
                jeu();
            break;
        // ----------------------------------
            case 2:
                regles();
            break;
        // ----------------------------------
            case 3:
                // Quitter
                continuer = 0;
                exit(-1);
            break;
        // ----------------------------------
            default:
                printf("Choix errone\n" );
        }
        // ----------------------------------
    } // while
    return 0;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------
    int menu()
    {
             int choix = 0;

             system("cls" ); // Nettoyer la console
             printf("======== MENU ========\n\n" );
             printf("(1) : Jouer\n" );
             printf("(2) : Regles du jeu\n" );
             printf("(3) : Quitter\n\n" );
             printf("======================\n\n\n" );
             printf("Que voulez vous faire ?\n\n" );
             scanf("%d", &choix);

             return choix;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------
    void regles()
    {
        int choix = 0;
        system("cls" ); //Nettoyer la console

        //Affichage des r�gles du Mastermind
        printf("***********************\n" );
        printf("*REGLES DU MASTERMIND:*\n" );
        printf("***********************\n" );

        printf("\nLe programme choisit une combinaison de 4 pions de couleurs au hasard\n" );
        printf("parmi 6 couleurs disponibles.\n" );
        printf("Cette combinaison de pions peut contenir plusieurs fois la meme couleur.\n" );
        printf("L utilisateur essaie de la deviner en proposant au maximum dix combinaisons.\n" );
        printf("Pour chaque tentative le programme indique le nombre de couleurs bien placees\n" );
        printf("(a l aide de pion noir) et le nombre de couleurs mal placees\n" );
        printf("(a l aide de pion blanc). Si le nombre maximal d essais est atteint,\n" );
        printf("le programme indique au joueur qu il a perdu et donne la combinaison.\n\n" );
        printf("------------------------------------------------------------------------------\n\n" );
        printf("Entrez (0) pour revenir au menu principale\n" );
        scanf("%d", &choix);

        if (choix == 0)
        {
            menu();
        }
        else
        {
            regles();
        }

    }
    // ------------------------------------------------------------------------------------------------------------------------------------------
        void jeu()
        {
            int essais=1,choix=0, ligne=9,continuer=1,verif=0,a; // "entrer" dans la boucle jeu
            char tab[10][8]={
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46},
                                {42,42,42,42,46,46,46,46}
                            };
            char comb[4];
                    system("cls" ); // Nettoyer la console
                    // --------------------------------------------------------------------------------------------------------
    // G�n�rer une combinaison al�atoire
    void initCode(int *codeSecret)
    {
    int i;
    srand (time(NULL));
    /* initialiser le code secret avec un choix de 6 couleurs */
    for (i = 0; i < 4 ; i++)
    {
     switch (rand() % 6)
     {
      case 0: codeSecret[i] = 'r'; break;
      case 1: codeSecret[i] = 'b'; break;
      case 2: codeSecret[i] = 'v'; break;
      case 3: codeSecret[i] = 'j'; break;
      case 4: codeSecret[i] = 'o'; break;
      case 5: codeSecret[i] = 'm'; break;
     }
    }
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------------
                // Jeu du MASTERMIND
                while (essais <= 10)
                {
                    int i, j, t, test=0, L=10, verif=0; // L : num�roter la ligne du tableau � partir de 10. ligne -> monter d'une ligne lorsque l'on entre une combinaison
                    char rep[4]={'\0'};
                    int a,b; // variable pour la v�rification des combinaisons entr�es
                    system("cls" ); // Nettoyer la console
                    printf("\nCouleurs disponibles : \n" );
                    printf("=========================================================\n" );
                    printf("r=Rouge - v=Vert - b=Bleu - j=Jaune - o=Orange - m=Marron \n" );
                    printf("=========================================================\n\n" );
                    // initialisation du tableau
                    for (i=0;i<=9;i++)
                    {
                        printf ("[%3d]-[",L);
                        for (j=0;j<=7;j++)
                        {
                            printf ("(%c)",tab[i][j]);
                        }
                        printf (" ]\n" );
                        L--;
                    }
                    //printf("\n comb : %s",comb);
                // --------------------------------------------------------------------------------------------------------
                    // Entrer une combinaison
                    while (verif != 4)
                    {
                        verif=0;
                        printf("\n\nEssai n# %d: \n",essais);
                        scanf("%s",&rep);
                        fflush(stdin);
                        // v�rification de la combinaison entr�e
                        for (a=0;a<=3;a++)
                        {
                            if (rep[a]=='r' || rep[a]=='v' || rep[a]=='b' || rep[a]=='j' || rep[a]=='o' || rep[a]=='m' )
                            {
                                verif++;
                            }
                            else
                            {
                                verif--;
                            }
                        }
                        if (verif !=4)
                        {
                            printf("\nCombinaison impossible. Recommencez !" );
                        }
                    }
                // -----------------------------------------------------------------------------------------------------------
                    // Ajout dans le combinaison dans le tableau
                    test = strcmp(rep,comb); // on compare les 2 chaine de caract�re, test sera n�gatif si s1 est < � s2, nul si s1 est == � s2 ou positif si s1 est > � s2
                    if (test==0)
                    {
                        essais=11; // le joueur a trouv� la combinaison, on quitte la boucle jeu
                    }
                    else
                    {
                        for (j=0;j<=3;j++) // se d�placer de cellule en cellule
                        {
                            for (t=0;t<=3;t++) // permet de se d�placer dans la chaine de caract�re comb, afin d'analyser chaque lettre
                            {
                                if (j == t) // si j == t, on est � la m�me position dans la chaine
                                {
                                    if (rep[j]==comb[t]) // Si le caract�re n�x est �gale au caract�re n�x de la combinaison
                                    {
                                        tab[ligne][j]=rep[j];
                                        tab[ligne][j+4]=78; // on affiche N (ascii 78)
                                    }
                                }
                                else
                                {
                                    if (rep[j]==comb[t]) // Si le caract�re n�x n'est pas �gale au caract�re n�x, mais qu'il figure dans la combinaison
                                    {
                                        tab[ligne][j]=rep[j];
                                        tab[ligne][j+4]=66; // on affiche B (ascii 66)
                                    }
                                    else // Sinon on affiche un point
                                    {
                                        tab[ligne][j]=rep[j];
                                        tab[ligne][j+4]=46; // on affiche un point (ascii 46)
                                    }
                                }
                            }
                        }
                    }
                    fflush(stdin);
                    essais++;
                    ligne--;
                }
                // ---------------------------------------------------------------------------------------
                // Jeu termin�
                if (essais == 12) // si essais == 12, alors le joueurs a trouv� la combinaison
                {
                    printf("Jeu termine" );
                    printf("\n\n  ****** B R A V O *****\n" );
                    printf("\n **!!! VOUS AVEZ TROUVE LA COMBINAISON !!!**\n" );
                    printf("\n\n*********************************************************\n" );
                    printf("---- Entrez (0) pour revenir au menu principal ou (1) pour rejouer : " );
                    scanf ("%d",&choix);
                }
                else
                {
                    system("cls" ); // Nettoyer la console
                    printf("Jeu termine" );
                    printf("\n\n*********************************************************\n" );
                    printf("---- Entrez (0) pour revenir au menu principal ou (1) pour rejouer : " );
                    scanf ("%d",&choix);
                }
                // ---------------------------------------------------------------------------------------
            if (choix == 0)
            {
                menu();
            }
            else
            {
                jeu();
            }
        }
