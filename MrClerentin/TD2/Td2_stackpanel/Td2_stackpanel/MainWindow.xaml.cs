﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Td2_stackpanel
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Point P0 = new Point(); //variable qui va enregister la valeur de l'ancier clic, elle est globale pour qu'on puisse toujours l'utiliser dans le programme
        bool premier_segment = true; //vrai car le premier sera forcement le premier segment

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Dessin_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Exécute lors du click sur le canvas
            Point P = Mouse.GetPosition(dessin); //Variable qui n'est pas globale, ne peut être utilisée que dans ce void
            //MessageBox.Show(P.X + ", " + P.Y);
            Line uneLigne = new Line();

            if (premier_segment == true)
            {
                P0.X = P.X; P0.Y = P.Y;

                premier_segment = false;
                return; //pour quitter le sous programme
            }
            switch (couleurs.SelectedIndex)
            {
                case 0:
                    uneLigne.Stroke = Brushes.Orange;
                    break; //obligatoire à chaque case
                case 1:
                    uneLigne.Stroke = Brushes.Red;
                    break;
                case 2:
                    uneLigne.Stroke = Brushes.Green;
                    break;
            }

            uneLigne.X1 = P0.X; uneLigne.Y1 = P0.Y;
            uneLigne.X2 = P.X; uneLigne.Y2 = P.Y;
            P0.X = P.X; P0.Y = P.Y;

            dessin.Children.Add(uneLigne);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            dessin.Children.Clear();
            premier_segment = true;
        }
    }
}
