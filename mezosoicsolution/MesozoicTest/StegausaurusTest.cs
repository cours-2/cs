﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicSolution;


namespace MesozoicTest
{
    [TestClass]
    public class StegausaurusTest
    {
        [TestMethod]
        public void TestStegausaurusConstructor()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestStegausaurusRoar()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Niarkniark", louis.roar());
        }

        [TestMethod]
        public void TestStegausaurusSayHello()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Je suis Louis, le Stego, j'ai 12 ans xptdr", louis.sayHello());
        }

        [TestMethod]
        public void TestStegausaurusHug()
            {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Dinosaur nessie = new Stegausaurus("Nessie", 12);

            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie", louis.hug(nessie));

        }
    }
}
