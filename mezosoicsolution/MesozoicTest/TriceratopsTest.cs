﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicSolution;


namespace MesozoicTest
{
    [TestClass]
    public class TriceratopsTest
    {
        [TestMethod]
        public void TestTriceratopsConstructor()
        {
            Triceratops louis = new Triceratops("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Triceratops", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestTriceratopsRoar()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual("Wallah", louis.roar());
        }

        [TestMethod]
        public void TestTriceratopsSayHello()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual("Je suis Louis, le Triceratops, j'ai 12 ans mamene", louis.sayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
            {
            Triceratops louis = new Triceratops("Louis", 12);
            Triceratops nessie = new Triceratops("Nessie", 12);

            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie", louis.hug(nessie));

        }
    }
}
