﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Diplodocus : Dinosaur
    {
        public Diplodocus(string name, int age) :
            base(name,"Diplodocus",age)
        { }
            protected override string specie { get { return "Diplodocus"; } }
        public override string roar()
        {
            return "lbblbllbbl";
        }

        public override string sayHello()
        {
            return String.Format("Je suis {0}, le Diplo, j'ai {1} ans wsh",this.name,this.age);
        }

    }
}
