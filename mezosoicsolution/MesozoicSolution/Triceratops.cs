﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
     public class Triceratops : Dinosaur
        {
            public Triceratops(string name, int age) :
                base(name, "Triceratops", age)
            {
            }
        protected override string specie { get { return "Triceratops"; } }
        public override string roar()
        {
            return "Wallah";
        }
        public override string sayHello()
        {
            return String.Format("Je suis {0}, le Triceratops, j'ai {1} ans mamene", this.name, this.age);
        }
    }
}
