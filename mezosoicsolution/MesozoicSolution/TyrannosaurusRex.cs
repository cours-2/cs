﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class TyrannosaurusRex : Dinosaur
    {
        public TyrannosaurusRex(string name, int age) :
            base(name, "TyrannosaurusRex", age)
        {
        }
        protected override string specie { get { return "TyrannosaurusRex"; } }
        public override string roar()
        {
            return "COUNTRY ROADS, TAKE ME HOOOOOME";
        }

        public override string sayHello()
        {
            return String.Format("Je suis {0}, le trex, j'ai {1} ans pepehands", this.name, this.age);
        }
    }
}
