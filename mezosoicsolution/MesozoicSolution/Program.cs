﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    class Program
    {
        static void Main(string[] args)
        {
            //Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Diplodocus("Nessie", 11);

            /*List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis);
            dinosaurs.Add(nessie);

            Console.WriteLine(dinosaurs.Count);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }
            */

            /*Hord cretace = new Hord();
            cretace.addDino(nessie);
            cretace.addDino(louis);

            cretace.sayHello();

            cretace.removeDino(louis);

            cretace.sayHello();*/

            Dinosaur henry = new Diplodocus("Henry", 11);
            Console.WriteLine(henry.sayHello());
            Dinosaur louis = new Stegausaurus("Louis", 12);
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(henry.sayHello());

           /* Laboratoire labo = new Laboratoire();
            Dinosaur Jean = labo.CreateDinosaur("Jean", "T-rex", 25);
            Console.WriteLine(Jean.sayHello());
            Dinosaur René = labo.CreateDinosaur("René", "Raptor", 2);
            Dinosaur Flo = labo.CreateDinosaur("Flo", "Dabosaurus", 20);
            Console.WriteLine(René.sayHello());
            Console.WriteLine(Flo.sayHello());*/


            Console.ReadKey();
        }
    }
}
