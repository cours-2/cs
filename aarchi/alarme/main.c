#include "Z:\Documents\aarchi\alarme\main.h"

//variables utiles a l'alarme
#define CODE_ARMEMENT 33  //Code pour armer
#define CODE_DESARMEMENT 1664  // Code pour d�sarmer
#define TEMPS_SORTIE 30  // Temps maximum pour sortir apr�s activation de l'alarme
#define TEMPS_ALARME 180   // TEMPS que dure l'alarme
#define TEMPS_Inactif 20   // Temps maximum pour d�sactiver l'alarme 

//effractions
#define EFRACTION1 input(pin_b1)  
#define EFRACTION2 input(pin_b2)
#define EFRACTION3 input(pin_b3)
#define EFRACTION4 input(pin_b4)
#define EFRACTION5 input(pin_b5)
#define EFRACTION6 input(pin_b6)

//activer ou desactiver
#define ALARME_ON output_high(pin_A1)
#define ALARME_OFF output_low(pin_A1)
#define SIRENE_ON output_high(pin_C0)
#define SIRENE_OFF output_low(pin_C0)
#define BUZZER_ON output_high(pin_A0)
#define BUZZER_OFF output_low(pin_A0)


//temoin des effractions
#define TEMOIN_ZONE1 output_high(pin_c1) 
#define TEMOIN_1_OFF output_low(pin_c1)

#define TEMOIN_ZONE2 output_high(pin_c2)
#define TEMOIN_2_OFF output_low(pin_c2)

#define TEMOIN_ZONE3 output_high(pin_c5)
#define TEMOIN_3_OFF output_low(pin_c5)

#define TEMOIN_ZONE4 output_high(pin_c4);
#define TEMOIN_4_OFF output_low(pin_c4);

#define TEMOIN_ZONE5 output_high(pin_e0);
#define TEMOIN_5_OFF output_low(pin_e0);

#define TEMOIN_ZONE6 output_high(pin_e1);
#define TEMOIN_6_OFF output_low(pin_e1);

//bool:
int1 Niveau_Alarme;    // Alarme active ou non
int1 ArmementActif;   // Actif ou non
int1 AlarmeActive;   //  Alarme sonne ou non
int1 Inactif;  // Porte active et qu'il faut une inactivit� 

int16 sec;  // Variable secondes
int16 dix;  // Variable dixi�me de secondes


int16 TimerActivation;
int16 TimeCompte;
int16 TimerAlarme;
int16 TimerInactif;
   
#int_TIMER1

void  TIMER1_isr(void) 
{
   set_timer1(3036);
   dix++;
   
   if(dix == 10)
   {
      sec++;
      
      if(AlarmeActive == 1)   // Alarme retentit
      {
         TimerAlarme++;
      }
      if(Inactif == 1)  //
      {
         TimerInactif++;
      }
      if((TimerActivation < TEMPS_SORTIE) && (ArmementActif == 1))  //Annuler l'armement apr�s l'activation du d�compte
      {
         TimerActivation++;
         TimerCompte = TEMPS_SORTIE - TimerActivation;
         printf("\r Temps : %lu s",TimerCompte);
         if(TimerCompte == 0)printf("\r Alarme activ�e");
      }
      dix = 0;
   }
}

#int_EXIT

void EXIT_isr(int16 val)
{                          
   output_high(pin_d0);
   output_low(pin_d1);
   output_low(pin_d2);
   
      if(input(pin_d3)){
         printf("\r1");
         val=((val*10) + 1);}
         
      if(input(pin_d4)){
         printf("\r4");
         val=((val*10) + 4);}
         
      if(input(pin_d5)){
         printf("\r7");
         val=((val*10) + 7);}
         
      if(input(pin_d6))
          printf("\r*");
      
   output_low(pin_d0);
   output_high(pin_d1);
   output_low(pin_d2);
   
      if(input(pin_d3)){
         printf("\r2");
         val=((val*10) + 2);}
         
      if(input(pin_d4)){
         printf("\r5");
         val=((val*10) + 5);}
         
      if(input(pin_d5)){
         printf("\r8");
         val=((val*10) + 8);}
         
      if(input(pin_d6)){
          printf("\r0");
          val=((val*10) + 0);}
      
   output_low(pin_d0);
   output_low(pin_d1);
   output_high(pin_d2);
   
      if(input(pin_d3)){
         printf("\r3");
         val=((val*10) + 3);}
         
      if(input(pin_d4)){
         printf("\r6");
         val=((val*10) + 6);}
         
      if(input(pin_d5)){
         printf("\r9");
         val=((val*10) + 9);}
         
      if(input(pin_d6)){
          printf("\r Code : %lu\r",val);
          
          if((val != CODE_DESARMEMENT) && (val != CODE_ARMEMENT))   // Si faux
          {
            printf("\rCode faux");
            printf("\rNouveaux code");
          }
         
          if(val == CODE_DESARMEMENT)   // Si code de d�sarmement correct
          {
            printf("\rAlarme desarmee");
            Niveau_Alarme = 0;
            ALARME_OFF
            Inactif = 0;
            BUZZER_OFF
            
            if( TEMPS_ALARME > TimerAlarme)
            {
               SIRENE_OFF
            }
          }
          
          if(val==CODE_ARMEMENT)  // code bon
          {
            if(val==CODE_ARMEMENT && ArmementActif==0)
            {
               printf("\rAlarme armee\rVous avez 30 secondes Puis taper � nouvez le code ");
               val=0;
               TEMOIN_1_OFF   // T�moins remis � leur �tat de base
               TEMOIN_2_OFF
               TEMOIN_3_OFF
               TEMOIN_4_OFF
               TEMOIN_5_OFF
               TEMOIN_6_OFF
               BUZZER_ON
               TimerActivation=0;
               ArmementActif=1;
               AlarmeActive = 0;
            }
            if(val==CODE_ARMEMENT && ArmementActif==1)  // Annulation de l'armement
            {
               printf("\rArmement remis � 0\r");
               TimerActivation = 0;
               BUZZER_OFF
               ArmementActif=0;
            }
          }
          val=0;
      }
     
   output_high(pin_d0);
   output_high(pin_d1);
   output_high(pin_d2);
}

void main()
{

   setup_adc_ports(NO_ANALOGS);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);
   setup_timer_2(T2_DISABLED,0,1);
   enable_interrupts(INT_TIMER1);
   enable_interrupts(INT_EXIT);
   enable_interrupts(GLOBAL);

   output_high(pin_d0);
   output_high(pin_d1);
   output_high(pin_d2);


   while(true)
   {
      if(TimerActivation == TEMPS_SORTIE)  // Sonner le temps de sortir
            {
               Niveau_Alarme = 1;
               ALARME_ON
               BUZZER_OFF
               ArmementActif=0;
               TimerActivation = 0;
            }
      if((Niveau_Alarme == 1) && (!EFRACTION3))
      {
         TEMOIN_ZONE3
         if(AlarmeActive == 0)
         {
            AlarmeActive = 1;
            SIRENE_ON;
            TimerAlarme = 0;
         }
      }
   
      if((Niveau_Alarme == 1) && (!EFRACTION1)) 
      {
         TEMOIN_ZONE1
         if(AlarmeActive == 0)
         {
            AlarmeActive = 1;
            SIRENE_ON;
            TimerAlarme = 0;
         }
      }
      if((Niveau_Alarme == 1) && (!EFRACTION2))
      {
         TEMOIN_ZONE2
         if(AlarmeActive == 0)
         {
            AlarmeActive = 1;
            SIRENE_ON;
            TimerAlarme = 0;
         }
      }
      
      if((Niveau_Alarme == 1) && (!EFRACTION4))
      {
         TEMOIN_ZONE4
         if(AlarmeActive == 0)
         {
            AlarmeActive = 1;
            SIRENE_ON;
            TimerAlarme = 0;
         }
      }
      
      if((Niveau_Alarme == 1) && (!EFRACTION6))
      {
         TEMOIN_ZONE6
         if(Inactif == 0)
         {
            printf("\rEntrer code pour mettre inactif, il vous reste 20 s");
            TimerInactif= 0;
            Inactif = 1;
            BUZZER_ON
         }
      }
   
      if(TimerInactif == TEMPS_Inactif)   // Si l'alarme de la porte n'est pas stopp�e,la sir�ne s'active
      {
         if(AlarmeActive == 0)
         {
            BUZZER_OFF
            AlarmeActive = 1;
            SIRENE_ON;
            TimerAlarme = 0;
            Inactif = 0;
         }
      }
      if((Niveau_Alarme == 1) && (!EFRACTION5))  
      {
         TEMOIN_ZONE5
         if(Inactif == 0)
         {
            printf("\rEntrer code Inactif, vous avez 20 s");
            TimerInactif = 0;
            Inactif = 1;
            BUZZER_ON
         }
      }

      if(TimerAlarme == TEMPS_ALARME)  // Temps de la sir�ne
      {
         SIRENE_OFF
      }
   }
}
