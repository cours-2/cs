#include <iostream>
#include <cstring>

using namespace std;

class Mystring
{
private:
    int *stat;
    int n;
    int spe;
    char* tab;
    void majstat();

public:
    Mystring();
    Mystring(char*);
    Mystring(char,int);

    ~Mystring();
    Mystring(const Mystring&);
    void affiche();
    char* Get_tab();
    void Maj();
    void Supprimer(char);
    void Doubler(char);
    void Concatener(Mystring);
    Mystring&operator=(const Mystring&);
    Mystring operator+(const Mystring&);
    //friend ostreau & operator << (ostreau &, Mystring &);//+
    Mystring operator -(const Mystring&);//+
};

void Mystring::Concatener(Mystring x)
{
    char* N;
    int k,j=0,i=0;
    k=(n+x.n)+1;
    N=new char[k];
    for(i=0; i<n; i++)
    {
        N[i]=tab[i];
    }
    for(i=n; i<=k; i++)
    {
        N[i]=x.tab[j];
        j++;
    }
    tab=new char[k];
    for(i=0; i<k; i++)
    {
        tab[i]=N[i];
    }
}

void Mystring::Doubler(char x)
{
    majstat();
    char* N;
    int j=0,i=0,car;
    car=stat[toupper(x)-'A'];
    N=new char[n+car];
    for(i=0; i<n; i++)
    {
        N[j]=tab[i];
        if(x==tab[i])
        {
            N[j+1]=tab[i];
            j++;
        }
        j++;
    }
    n=n+car;
    tab=new char[n];
    for(i=0; i<n; i++)
    {
        tab[i]=N[i];
    }
}

void Mystring::Supprimer(char x)
{
    char N[n];
    int j=0;
    for (int i=0; i<n; i++)
    {
        if(x!=tab[i])
        {
            N[j]=tab[i];
            j++;
        }
    }

}

void Mystring::majstat()
{
    spe=0;
    for(int i=0; i<26; i++)
        stat[i]=0;
    for(int i=0; i<n; i++)
    {
        if((toupper(tab[i])>='A')&&(toupper(tab[i])<='Z'))
            stat[toupper(tab[i])-'A']++;
        else
            spe++;
    }
}

Mystring::Mystring()
{
    tab=NULL;
    n=0;
    spe=0;
    for(int i=0; i<26; i++)
        stat[i]=0;

}

Mystring :: Mystring (char *pch)
{
    n=strlen(pch);
    tab=new char [n+1];
    strcpy(tab,pch);
    stat=new int [26];
    majstat();
}

//Mystring :: Mystring(int pn, Mystring :: Mystring (const Mystring & s)char pch1)

//{
    //int i;
    //n=pn;
    //tab=new char [n+1];

    //for(i=0;i<n;i++)
    //{
        //tab[i]=pch1;
    //}
    //tab[i]='\0';
    //:stat=new int [26];
    //majstat();
//}

//Mystring::~Mystring()
//{
    //cout<<"destructeur"<<endl;
    //delete tab;
    //delete stat;
//}

void Mystring::affiche()
{
    majstat();
    for(int i=0; i<n; i++)
        cout<<tab[i];
    cout<<endl;
    for(int cul=0; cul<26; cul++)
    {
        cout<<char(cul+'A')<<":"<<stat[cul]<<endl;
    }
}

Mystring :: ~Mystring()
{
    cout<<"destructeur"<<endl;
    if (tab!=NULL) delete tab;
    delete stat;
}

Mystring Mystring::operator+(const Mystring& s){
    char* N;
    int k,j=0,i=0;
    k=(n+s.n)+1;
    N=new char[k];
    for(i=0; i<n; i++)
    {
        N[i]=tab[i];
    }
    for(i=n; i<=k; i++)
    {
        N[i]=s.tab[j];
        j++;
    }
    Mystring st(N);
    return st;
}



Mystring& Mystring::operator=(const Mystring& s){
    if (this != &s)
    {
        delete tab;
        tab=new char[s.n+1];
        n=s.n;
        for(int i=0; i<=n; i++)
            tab[i]=s.tab[i];
    }
     return *this;
}

//Mystring Mystring::operator-(const Mystring& car)
//{
    //Mystring res (*this);
    //res.Supprimer (car);

    //return res;
//}

void Mystring::Maj()
{
    for(int i=0; i<n; i++)
        tab[i]=toupper(tab[i]);
}



Mystring::Mystring(const Mystring& s)
{
    tab=new char[s.n+1];
    n=s.n;
    for(int i=0; i<=n; i++)
        tab[i]=s.tab[i];
}

Mystring::Mystring(char a, int b)
{
    tab=new char[b];
    for (int i=0; i<b; i++)
    {
        tab[i]=a;
    }
    n=b;
}

//Mystring::Mystring(char* p)
//{
    //int i=0;
    //do
    //{
        //i++;
    //}
    //while(p[i]!='\0');
    //tab=new char [i];
    //n=i;
    //for(int j=0; j<=n; j++)
    //{
        //tab[j]=p[j];
    //}
//}

char* Mystring::Get_tab()
{
    return tab;
}

int main()
{

    Mystring s1("int");
    Mystring s2(10,'a');
    Mystring s3();//'u',6);
    Mystring s4(s1);
    s1.affiche();
    s2.affiche();
//    s3.affiche();

//    Mystring s1 ("int"), s2("info");//, //s3("salut");//+
//    Mystring *s1 = new Mystring ("int");//+
//    s1 -> affiche();//+
//    s3 = s1+s2;
    //s3.operator = (s1.operator + (s2));//+

//   MyString s2(tmp); //("abc");
    s2.affiche();
    s1.Supprimer('i');
    s1.Doubler('u');
    s2.Concatener(s1);

   //MyString s3(4,'b');
   //s3.affiche();

//    cout<<"str 2: "<<s2.Get_tab()<<" str 3: "<<s3.Get_tab()<<endl;
//    s3.Maj();
//    cout<<"str 3:"<<s3.Get_tab()<<endl;
    s2.Supprimer('e');
    cout<<"str 2:"<<s2.Get_tab()<<endl;
    s2.Doubler('s');
//    s3.Doubler('U');
    cout<<"str 2:"<<s2.Get_tab()<<endl;
//    s2.Concatener(s3);
    cout<<"str 2:"<<s2.Get_tab()<<endl;
//    cout<<"str 3:"<<s3.Get_tab()<<endl;
//    s2=s3;
    cout<<"str 2:"<<s2.Get_tab()<<endl;
//    s3=s2+s4;
//    cout<<"str 3:"<<s3.Get_tab()<<endl;

    char tmp[]="abc";

    return 0;//+
}
