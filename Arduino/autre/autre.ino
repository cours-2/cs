/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/
//premier exo avec potentiometre et led
//int LED=4;
//int POT = 0;
// the setup function runs once when you press reset or power the board
//void setup() {
  // initialize digital pin LED_BUILTIN as an output.
 // pinMode(LED, OUTPUT);
  //Serial.begin(9600);
//}

// the loop function runs over and over again forever
//void loop() {
//Serial.println("coucou");
//digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
                      // wait for a second
//digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
     // wait for a second

  //Serial.println(analogRead(POT));
     // wait for a second
//}
//----------------------------------------------------------------------------------------//
//programme srvo moteur
//int potentiometre = A0;//potentiomètre connecté sur A0 de la carte
//int led = 3;// led connectée sur PWM 3 de la carte

//int valeurpotar = 0;//variable valeur du potentiomètre
//int valeurled = 0;//variable valeur de la led

//void setup() {

//Serial.begin(9600);
//Serial.println ("Bienvenue sur les tutoriels d'IHM 3D");
//pinMode (led , OUTPUT);// led déclarée en sortie
//}
//void loop() {

//valeurpotar = analogRead(potentiometre);//stockage de la valeur du potentiomètre dans valeur potar

//valeurled = map(valeurpotar, 0, 1023, 0, 255);//produit en croix 0 --> 0 / 1023 --> 255

//analogWrite(led, valeurled);

//information sur le moniteur série

//Serial.print("POTENTIOMETRE :");
//Serial.println(valeurpotar);
//Serial.print("LED : ");
//Serial.println(valeurled);
//Serial.print ("");

//delay (750);// delay pour avoir le temps de lire la valeur sur le moniteur série.
//}
//-------------------------------------------------------------------------------------------//
//afficheur 7segments

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
//----- Adressage matériel -----
// En cas de non fonctionnement, mettez la ligne 8 en
// commentaire et retirez le commentaire à la ligne 9.
LiquidCrystal_I2C lcd(0x27, 20, 4);
//LiquidCrystal_I2C lcd(0x3F,20,4);
void setup()
{
 lcd.init(); // initialisation de l'afficheur
}
void loop()
{
 lcd.backlight();
 // Envoi du message
 lcd.setCursor(0, 0);
 lcd.print(" Go Tronic");
 lcd.setCursor(0,1);
 lcd.print(" I2C Serial LCD");
}

